<?php

    require_once("./credentials.php");

    $jsonData = file_get_contents('php://input');
    $data = json_decode($jsonData, true);

    if ($data !== null) {
        $chf = $data['chf'];
        $usd = $data['usd'];
        $eur = $data['eur'];
        $gbp = $data['gbp'];
        $date = $data['date'];
     }

    $conn = new mysqli($host, $username, $password, $db);

    if ($conn->connect_error) {
        die("ERR1" . $conn->connect_error);
    }

    $sql = "INSERT INTO `exchangerates`(`id`, `dateOfRate`, `usdRate`, `chfRate`, `gbpRate`, `eurRate`) VALUES (null, '$date', '$usd', '$chf', '$gbp', '$eur')";

    try{
        $result = $conn->query($sql);
        echo $result;
    }catch(Exception $e){
        echo "ERR";
    }finally{
        $conn->close();
    }


?>
