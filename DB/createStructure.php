<?php

    require_once("./credentials.php");

    $conn = new mysqli($host, $username, $password, $db);

    if ($conn->connect_error) {
        die("ERR1" . $conn->connect_error);
    }

    $sql = "
        CREATE DATABASE IF NOT EXISTS hotpinkKantor;
        USE hotpinkKantor;
        CREATE TABLE IF NOT EXISTS exchangerates(
            id INT PRIMARY KEY AUTO_INCREMENT, 
            dateOfRate DATE,
            usdRate DECIMAL(6,4),
            chfRate DECIMAL(6,4),
            gbpRate DECIMAL(6,4),
            eurRate DECIMAL(6,4)
        );
    ";

    if ($conn->multi_query($sql) === TRUE) {
        echo "OK";
    } else {
        echo "ERR2" . $conn->error;
    }

    $conn->close();
?>
