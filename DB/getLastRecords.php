<?php

    require_once("./credentials.php");

    $jsonData = file_get_contents('php://input');
    $data = json_decode($jsonData, true);

    if ($data !== null) {
        $days = $data['days'];
    }else{
        $days = 7;
    }

    $conn = new mysqli($host, $username, $password, $db);

    if ($conn->connect_error) die(json_encode(array("ERR"=>"Cannot establish connection")) . $conn->connect_error);

    $sql = "SELECT * FROM exchangerates ORDER BY dateOfRate DESC LIMIT $days";

    try{
        $result = $conn->query($sql);
        $rows = [];
        while($row = $result->fetch_assoc()) array_push($rows, $row);
        if($rows!=null) {
            if(sizeof($rows)>0) print_r(json_encode($rows));
        }
        else print_r(json_encode(array("ERR"=>"No results")));
    }catch(Exception $e){
        echo json_encode(array("ERR"=>"Invalid query"));
    }finally{
        $conn->close();
    }

?>
