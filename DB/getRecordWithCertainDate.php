<?php
    require_once("./credentials.php");

    $conn = new mysqli($host, $username, $password, $db);

    $jsonData = file_get_contents('php://input');
    $data = json_decode($jsonData, true);

    if ($data !== null) {
        $date = $data['date'];
     }

    if ($conn->connect_error) die(json_encode(array("ERR"=>"Cannot establish connection")) . $conn->connect_error);

    $sql = "SELECT * FROM exchangerates WHERE exchangerates.dateOfRate = '$date';";

    try{
        $result = $conn->query($sql);
		$row = $result->fetch_assoc();
        if($row!=null) {
            if(sizeof($row)>0) print_r(json_encode($row));
        }
        else print_r(json_encode(array("ERR"=>"No results")));
    }catch(Exception $e){
        echo json_encode(array("ERR"=>"Invalid query"));
    }finally{
        $conn->close();
    }
?>
