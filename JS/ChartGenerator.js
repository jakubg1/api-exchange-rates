class ChartGenerator{
    static destroyExistingChart() {
        const chartInstance = Chart.getChart($('#myCanvasChart').getContext('2d'));
        if (chartInstance) {
            chartInstance.destroy();
        }
    }

	constructor(sets, dates) {

        const plugin = {
            id: 'customCanvasBackgroundColor',
            beforeDraw: (chart, args, options) => {
              const {ctx} = chart;
              ctx.save();
              ctx.globalCompositeOperation = 'destination-over';
              ctx.fillStyle = options.color || '#99ffff';
              ctx.fillRect(0, 0, chart.width, chart.height);
              ctx.restore();
            }
          };

        const ctx = $('#myCanvasChart').getContext('2d');

        const chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: dates,
                datasets: sets
            },
            options: {
                plugins: {
                  customCanvasBackgroundColor: {
                    color: 'rgb(45,45,45)',
                  },
                  legend:{
                    labels:{
                        color: 'white',
                    }
                  }
                },
                scales: {
                    x: {
                        type: 'time',
                        time: {
                            parser: 'YYYY-MM-DD', 
                            unit: 'day',
                            tooltipFormat: 'MMM D',     
                            displayFormats: {
                                day: 'MMM D'
                            }
                        },
                        ticks:{
                            color: "white"
                        }
                    },
                    y: {
                        beginAtZero: false,
                        ticks:{
                            color: "white",
                        }
                    }
                }
            },
            
            plugins: [plugin],
            
        });
	}
}