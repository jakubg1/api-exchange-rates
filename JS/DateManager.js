class DateManager{
	static getDate = (date) => {
		date = new Date(date);
		const year = date.getFullYear()
		const month = String(date.getMonth() + 1).padStart(2, '0')
		const day = String(date.getDate()).padStart(2, '0')

		return `${year}-${month}-${day}`
	}

	static getToday = () => {
		let date = new Date();
		const year = date.getFullYear()
		const month = String(date.getMonth() + 1).padStart(2, '0')
		let day = String(date.getDate()).padStart(2, '0')
		if (date.getDay() == 6) day -= 1
		else if (date.getDay() == 0) day -= 2
		return `${year}-${month}-${day}`
	}

	static calculateDaysBetweenDates = (date1, date2) => {
		const firstDate = new Date(date1);
		const secondDate = new Date(date2);
		const timeDifference = secondDate - firstDate;
		const daysDifference = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
		return daysDifference;
	}

	static isWorkingDay = (date) => {
		const checkedDate = new Date(date);
		if (checkedDate.getDay() != 6 && checkedDate.getDay() != 0) return true
		return false
	} 

	static isInHistory = (date) => {
		const checkedDate = new Date(date);
		const today = new Date()
		if (DateManager.getDate(checkedDate) < DateManager.getDate(today) || (DateManager.getDate(checkedDate) == DateManager.getDate(today) && today.getHours() >= 14) ) return true
		return false
	} 
}