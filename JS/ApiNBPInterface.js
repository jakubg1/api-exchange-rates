class ApiNBPInterface {

	currentData = async () => {
		let currenciesValues = await fetch("https://api.nbp.pl/api/exchangerates/tables/a?format=json")
			.then(data => data.json())

		return currenciesValues[0]
	}

	dataFromDate = async (date) => {
		let currenciesValues = await fetch(`https://api.nbp.pl/api/exchangerates/tables/a/${DateManager.getDate(date)}?format=json`)
			.then(data => data.json())
			.catch(err => [false])

		return currenciesValues[0]
	}

}