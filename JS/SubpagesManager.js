

class SubpagesManager{
	status = 0;

	constructor(){
		this.putEventListenersOn()
	}

	putEventListenersOn(){
		$(".title").addEventListener("click", () => this.changeContent(0));

		$("#rates").addEventListener("click", () => this.changeContent(1));
		$("#navRates").addEventListener("click", () => this.changeContent(1));

		$("#charts").addEventListener("click", () => this.changeContent(2));
		$("#navCharts").addEventListener("click", () => this.changeContent(2));

	}

	changeContent(stat){
		if (stat != this.status){
			this.status = stat

			switch (stat) {
				case 0:
					$(".beginView").classList.add("flex")
					$(".ratesBox").classList.remove("flex")
					$(".charts").classList.remove("flex")
					break;
				case 1:
					$(".ratesBox").classList.add("flex")
					$(".beginView").classList.remove("flex")
					$(".charts").classList.remove("flex")

					break;
				case 2:
					$(".beginView").classList.remove("flex")
					$(".ratesBox").classList.remove("flex")
					$(".charts").classList.add("flex")
					break;
				default:
					break;
			}
		}
	}

}