$ = (selector) => {return document.querySelector(selector)}

class WebManager{

	usd = 0
	gbp = 0
	chf = 0
	eur = 0
	values = []
	realEstimation
	realSource
	todayDate = ''
	currentSource = 0

	showValuesInConsole = async () => {
		await console.log(this.values)
		await console.log(this.todayDate)

		await console.log("USD: "+String(this.usd))
		await console.log("EUR: "+String(this.eur))
		await console.log("CHF: "+String(this.chf))
		await console.log("GBP: "+String(this.gbp))
	}

	getCurrencyValue = async (currency = "USD") => {
		let currencyValue = this.values.filter(object => object.code == currency)
		return currencyValue[0].mid
	}

	putEventsOnElements = () => {

		$("#firstValue").oninput = () => {
			this.currentSource = 0
			this.updateInputs()
		}
		$("#secondValue").oninput = () => {
			this.currentSource = 1
			this.updateInputs()
		}

		$("#firstSelect").oninput = this.updateInputs
		$("#secondSelect").oninput = this.updateInputs

		$("#date").oninput = async () => {
			if (!DateManager.isWorkingDay($("#date").value)){
				await this.showAlert("Wybrałeś sobotę lub niedzielę.")
			}else if(!DateManager.isInHistory($("#date").value)){
				await this.showAlert("Wybrałeś dzień, który jeszcze nie był (lub dane nie zostały jeszcze udostępnione).")
			}else{
				await this.updateExchangeRates(DateManager.getDate($("#date").value));
				await this.updateInputs()
				await this.showAlert(false)
			}
		}

		$("#lastThreeDays").onclick = () => {
			ChartGenerator.destroyExistingChart()
			this.generateNewChart(3)
		}
		$("#lastFiveDays").onclick = () => {
			ChartGenerator.destroyExistingChart()
			this.generateNewChart(5)
		}
		$("#lastWeek").onclick = () => {
			ChartGenerator.destroyExistingChart()
			this.generateNewChart(7)
		}
	}

	showAlert = async (content) => {
		if(content == false){
			$(".communicateBox").innerHTML = "";
			$(".communicateBox").style.display = "none";
		}else{
			$(".communicateBox").innerHTML = content;
			$(".communicateBox").style.display = "unset";

		}
	}

	updateData = async (certainDate) => {
		if(!certainDate){
			let dataFromDB = await this.dbManager.getLastRecord()
			let typedDate = await DateManager.getToday()
			if(dataFromDB.dateOfRate == typedDate){
				this.todayDate = dataFromDB.dateOfRate
				this.values = [
					{code: "USD", mid:dataFromDB.usdRate},
					{code: "EUR", mid:dataFromDB.eurRate},
					{code: "CHF", mid:dataFromDB.chfRate},
					{code: "GBP", mid:dataFromDB.gbpRate},
				]
				// console.log("Data loaded from Database (today)")
			}else{
				let currencies = await this.apiInterface.currentData()
				this.values = currencies.rates;
				this.todayDate = currencies.effectiveDate;
				// console.log("Data loaded from API (today)")
			}
		}else{
			let dataFromDB = await this.dbManager.getRecordWithCertainDate(certainDate)
			if (dataFromDB != null){
				this.values = [
					{code: "USD", mid:dataFromDB.usdRate},
					{code: "EUR", mid:dataFromDB.eurRate},
					{code: "CHF", mid:dataFromDB.chfRate},
					{code: "GBP", mid:dataFromDB.gbpRate},
				]
				// console.log("Data loaded from Database (certain date)")
			}else{
				let currencies = await this.apiInterface.dataFromDate(certainDate)
				// console.log("Data loaded from API (certain date)")
				if(currencies) this.values = currencies.rates;
				else await alert("Brak danych dostępnych na ten dzień")
			}
		}
	}

	updateExchangeRates = async (certainDate = false) => {
		await this.updateData(certainDate)
		this.usd = await this.getCurrencyValue("USD")
		this.chf = await this.getCurrencyValue("CHF")
		this.eur = await this.getCurrencyValue("EUR")
		this.gbp = await this.getCurrencyValue("GBP")
		await this.putEventsOnElements()
	}

	updateInputs = async () => {
		let inZlotys

		if(this.currentSource == 0){
			let firstVal = $("#firstValue").value

			if ($("#secondSelect").value=="PLN") $("#secondValue").value = (firstVal*(await this.getCurrencyValue($("#firstSelect").value))).toFixed(4)
			else{
				if($("#firstSelect").value=="PLN") inZlotys = firstVal
				else inZlotys = $("#firstValue").value*(await this.getCurrencyValue($("#firstSelect").value))
				$("#secondValue").value = (inZlotys/(await this.getCurrencyValue($("#secondSelect").value))).toFixed(4)
			}
		}else if(this.currentSource == 1){
			let firstVal = $("#secondValue").value

			if ($("#firstSelect").value=="PLN") $("#firstValue").value = (firstVal*(await this.getCurrencyValue($("#secondSelect").value))).toFixed(4)
			else{
				if($("#secondSelect").value=="PLN") inZlotys = firstVal
				else inZlotys = $("#secondValue").value*(await this.getCurrencyValue($("#secondSelect").value))
				$("#firstValue").value = (inZlotys/(await this.getCurrencyValue($("#firstSelect").value))).toFixed(4)
			}
		}
		
	}

	insertRatesFromCertainDate = async(date) => {
		// await console.log(date)
		let currenciesValues = await this.apiInterface.dataFromDate(date)
		if (currenciesValues){

			let currencies = currenciesValues.rates
			let thatDay = currenciesValues.effectiveDate
			
			let Tusd  = currencies.filter(object => object.code == "USD")[0].mid
			let Tgbp  = currencies.filter(object => object.code == "GBP")[0].mid
			let Teur  = currencies.filter(object => object.code == "EUR")[0].mid
			let Tchf  = currencies.filter(object => object.code == "CHF")[0].mid
			
			await this.dbManager.insertRate(Tusd, Tchf, Teur, Tgbp, thatDay);
		}
	}

	updateRowsBetweenDates = async (theOldestDate, yesterday) => {
		let difference = await DateManager.calculateDaysBetweenDates(theOldestDate, yesterday) - 1
		let currentDate = new Date(theOldestDate)
		while (difference > 0){
			currentDate.setDate(currentDate.getDate() + 1)
			if (DateManager.isWorkingDay(currentDate)) {
				// console.log("Requesting: " + String(currentDate))
				await this.insertRatesFromCertainDate(currentDate)
			}else{
				// console.log("Skipped: " + String(currentDate))
			}
			difference-=1
		}
	}

	updateIfRecordIsNotInDB = async () => {
		let lastRecord  = await this.dbManager.getLastRecord()
		if (lastRecord != null){
			if (lastRecord.dateOfRate != this.todayDate){
				await this.updateRowsBetweenDates(lastRecord.dateOfRate, this.todayDate)
				await this.dbManager.insertRate(this.usd, this.chf, this.eur, this.gbp, this.todayDate)
			}
		}else this.dbManager.insertRate(this.usd, this.chf, this.eur, this.gbp, this.todayDate)
	}

	generateNewChart = async (lastDays) => {
		
		lastDays = await this.dbManager.getLastRecords(lastDays)
		let usdSet = [], chfSet = [], gbpSet = [], eurSet = [], datesSet = [];
		lastDays.forEach(async (obj) => {
			usdSet.push(obj.usdRate)
			eurSet.push(obj.eurRate)
			gbpSet.push(obj.gbpRate)
			chfSet.push(obj.chfRate)

			datesSet.push(obj.dateOfRate)
		})

		const borderWidth = 3;
		this.chartGen = new ChartGenerator([{
				label: 'USD',
				data: usdSet,
				borderColor: 'orange',
				borderWidth: borderWidth,
				fill: false
			},{
				label: 'EUR',
				data: eurSet,
				borderColor: 'blue',
				borderWidth: borderWidth,
				fill: false
			},{
				label: 'GBP',
				data: gbpSet,
				borderColor: 'green',
				borderWidth: borderWidth,
				fill: false
			},{
				label: 'CHF',
				data: chfSet,
				borderColor: 'red',
				borderWidth: borderWidth,
				fill: true 
			}
		], datesSet)
	}

	initialize = async () => {
        const today = new Date().toISOString().split('T')[0]
        document.getElementById('date').value = today
		
		this.apiInterface = new ApiNBPInterface()
		this.dbManager = new DbManager()
		this.ctManager = new SubpagesManager()
		this.generateNewChart(7)
		
		let strutureStatus = await this.dbManager.createDbStructure()
		if (strutureStatus == false) alert("Could not create db or connect to DBMS")
		
		await this.updateExchangeRates()
		await this.updateInputs()
		await this.updateIfRecordIsNotInDB()
			   
	}

	constructor() {
		this.initialize()
	}



}

