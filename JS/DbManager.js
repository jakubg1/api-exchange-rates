class DbManager{
	
	createDbStructure = async () => {
		let createStructureStatus= await fetch("./DB/createStructure.php")
			.then(data => data.text())
		if (createStructureStatus == "OK") return true
		else return false
	}

	insertRate = async (insertUSD = 0, insertCHF = 0, insertEUR = 0, insertGBP = 0, insertDate = "0000-00-00") => {
		let insertRateStatus = await fetch("./DB/insertRate.php", {
			method: "POST",
			body: JSON.stringify({
				"chf":insertCHF,
				"usd":insertUSD,
				"eur":insertEUR,
				"gbp":insertGBP,
				"date":insertDate
			})
		})
			.then(data => data.text())
		if (insertRateStatus != "ERR") return true
		else{
			console.log("Could not insert row.")
			return false
		} 
	}

	getLastRecord = async () => {
		let lastRecord = await fetch("./DB/getLastRecord.php")
			.then(data => data.json())
			.catch(error => {return null})
		if (lastRecord != null) {
			if (lastRecord.ERR) {
				console.log("no records")
				return null
			}
		}
		return lastRecord
	}

	getRecordWithCertainDate = async (date) => {
		let record = await fetch("./DB/getRecordWithCertainDate.php", {
			method: "POST",
			body: JSON.stringify({
				"date":date
			})
		})
			.then(data => data.json())
			.catch(error => {return null})
		if (record != null) {
			if (record.ERR) {
				console.log("no records")
				return null
			}
		}
		return record
	}

	getLastRecords = async (days) => {
		let lastRecords = await fetch("./DB/getLastRecords.php", {
			method: "POST",
			body: JSON.stringify({
				"days":days
			})
		})
			.then(data => data.json())
			.catch(error => {return null})
		if (lastRecords != null) {
			if (lastRecords.ERR || lastRecords.length == 0) {
				console.log("no records")
				return null
			}
		}
		return lastRecords
	}

}